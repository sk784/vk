package myapplications.libraries.vk.ui.image;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import myapplications.libraries.vk.mvp.model.image.IImageLoader;

public class PicassoImageLoader implements IImageLoader<ImageView> {
    @Override
    public void loadInto(String url, ImageView container) {
        Picasso.get().load(url).into(container);
    }
}
