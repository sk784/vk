package myapplications.libraries.vk.mvp.presenter;

import android.annotation.SuppressLint;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import io.reactivex.Scheduler;
import myapplications.libraries.vk.mvp.model.repo.UserRepo;
import myapplications.libraries.vk.mvp.view.MainView;


@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private UserRepo userRepo;
    private Scheduler mainThreadScheduler;

    public MainPresenter(Scheduler mainThreadScheduler) {
        this.userRepo = new UserRepo();
        this.mainThreadScheduler = mainThreadScheduler;
    }


    @SuppressLint("CheckResult")
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().init();
        loadUser();
       // loadDataWithOkHttp();
    }


    @SuppressLint("CheckResult")
    private void loadUser() {
        getViewState().showLoading();
        userRepo.getUser()
                .observeOn(mainThreadScheduler)
                .subscribe(response -> {
                    getViewState().hideLoading();
                    getViewState().setUserName(response.getResponse().getFirstName()+" "+ response.getResponse().getSecondName());
                    getViewState().setUserLastSeen(response.getResponse().getLastSeen().getLastSeenTime());
                    getViewState().setUserCity(response.getResponse().getCity().getCityTitle);
                    getViewState().setUserBirthday(response.getResponse().getBirthDay());
                    getViewState().setUserEducation(response.getResponse().getEducation());
                    getViewState().loadImage(response.getResponse().getUserPhoto());
                }, t -> getViewState().hideLoading());
    }

//    @SuppressLint("CheckResult")
//    private void loadDataWithOkHttp() {
//        Single<String> single = Single.fromCallable(() -> {
//            OkHttpClient client = new OkHttpClient();
//            Request request = new Request.Builder()
//                    .url("https://api.vk.com/method/users.get?user_id=2320085&fields=bdate&access_token=935c4d3a935c4d3a935c4d3ad3933180589935c935c4d3ace9cc622cdef42a4f21cd1e7&v=5.102")
//                    .build();
//
//            return client.newCall(request).execute().body().string();
//        });
//
//        single.subscribeOn(Schedulers.io())
//                .subscribe(string -> Timber.d(string));
//    }
}
