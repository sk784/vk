package myapplications.libraries.vk.mvp.model.repo;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import myapplications.libraries.vk.mvp.model.api.ApiHolder;
import myapplications.libraries.vk.mvp.model.api.model.ProfileInfo;


public class UserRepo {

    private String keyApi = "935c4d3a935c4d3a935c4d3ad3933180589935c935c4d3ace9cc622cdef42a4f21cd1e7";
    private String apiVersion = "5.102";


    public Single<ProfileInfo> getUser() {

        return ApiHolder.getApi().getProfileInfo("2320085","bdate",keyApi,apiVersion)
                .subscribeOn(Schedulers.io());
    }
}
