package myapplications.libraries.vk.mvp.model.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import timber.log.Timber;

public class ProfileInfo {

    @SerializedName("response")
    private ArrayList<UserData> response = new ArrayList<>();

    public ProfileInfo(ArrayList<UserData> response) {
        this.response = response;
    }


    public ProfileInfo() {
        this.response = response;
    }

    public ArrayList<UserData> getResponse() {
        Timber.d("3123 ");
        return response;
    }

    public void setResponse(ArrayList<UserData> response) {
        this.response = response;
    }
}